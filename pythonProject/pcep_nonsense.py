print("Hisssssss...")

print("Hello!")

print("Hello, World!")

# function_name(argument)   functions must always have double parenth + an argument

print("Hello Python!")

print("Joshua")

print("The itsy bitsy spider climbed up the waterspout.")
print("Down came the rain and washed the spider out.")

print("The itsy bitsy spider climbed up the waterspout.")
print()
print("Down came the rain and washed the spider out.")

print("The itsy bitsy spider\n climbed up the waterspout.")
print()
print("Down came the rain\n and washed the spider out.")

print("\\")

print("The itsy bitsy spider", "climbed up", "the waterspout.")

print("My name is", "Python.")
print("Monty Python.")

print("My name is", "Python.", end=" ") # holy crap something NEW!!
print("Monty Python.")  # end seems to somehow concatenate these two lines??

print("My name is ", end="") # set to no characters, not even spaces
print("Monty Python.")

print("My", "name", "is", "Monty", "Python.", sep="-") # like separator; works kind of like a delimiter!

print("My", "name", "is", sep="_", end="*")
print("Monty", "Python.", sep="*", end="*\n")



